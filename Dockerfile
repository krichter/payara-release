FROM maven:slim
RUN apt-get update && apt-get install --yes wget
RUN wget https://s3-eu-west-1.amazonaws.com/payara.fish/Payara+Downloads/5.182/payara-5.182.tar.gz && tar xf payara-5.182.tar.gz
# starting the server has no effect when running the image
